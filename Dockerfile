FROM docker:dind
MAINTAINER francisco.luis.morais@gmail.com

RUN which ssh-agent || ( apk update && apk add --no-cache openssh )
RUN eval $(ssh-agent -s)

ENV NODE_VERSION 10.20.0

RUN mkdir -p ~/.ssh
RUN chmod 700 ~/.ssh
RUN touch ~/.ssh/known_hosts
RUN chmod 644 ~/.ssh/known_hosts
RUN apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/main openssl
RUN apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing gdal-dev

RUN apk add curl

RUN apk update
RUN apk --no-cache add --virtual builds-deps build-base python gcc git
RUN apk add npm
RUN apk add nodejs
RUN apk add bash
RUN apk add --no-cache py-pip git
RUN npm config set python /usr/bin/python
RUN npm rebuild bcrypt --build-from-source
RUN npm install yarn -g
RUN npm install rimraf -g
RUN npm install knex -g
RUN npm install cross-env -g
RUN npm i -g npm
RUN npm install
RUN npm install flow -g
RUN apk add python2-dev
RUN apk add python3-dev
RUN apk add libffi-dev
RUN apk add openssl-dev
RUN pip install docker-compose
RUN echo http://dl-2.alpinelinux.org/alpine/edge/community/ >> /etc/apk/repositories
RUN apk --no-cache add shadow
RUN apk add --update bash

COPY id_rsa ./id_rsa
COPY id_rsa.pub ./id_rsa.pub
COPY .bashrc /root

ENV DOCKER_API 1.28
ENV DOCKER_HOST tcp://127.0.0.1:2375

CMD ['sh', '/usr/local/bin/dockerd-entrypoint.sh', '&']
