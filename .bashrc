#!/bin/sh
sh /usr/local/bin/dockerd-entrypoint.sh &
sleep 2
echo $DOCKER_HOST
which ssh-agent || ( apk update && apk add --no-cache openssh )
eval $(ssh-agent -s)
mkdir -p ~/.ssh
ls -la
cp /id_rsa ~/.ssh
cp /id_rsa.pub ~/.ssh
chmod -R 700 ~/.ssh
echo "$(cat /id_rsa)" | tr -d "\r" | ssh-add - > /dev/null
touch ~/.ssh/known_hosts
ssh-keyscan -H gitlab.com > ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts
